package api

import (
	"net/http"
	"github.com/google/go-querystring/query"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/url"
	"reflect"
	"time"
)

// An ErrorResponse reports the error caused by an API request
type ErrorResponse struct {
	// HTTP response that caused this error
	Response *http.Response
	// Error message
	Message string `json:"message"`
}

type Client struct {
	//httpClient  *http.Client
	EndpointURL string
	UrlQuery    interface{}
	req         *http.Request
}

//GET - compose and execute request
func (c *ClientNasa) Get(endpointURL string, urlQuery interface{}) (jsonResponse interface{}, err error) {
	client := Client{
		EndpointURL: endpointURL,
		UrlQuery:    urlQuery,
	}

	err = client.newRequest()
	if err != nil {
		return nil, err
	}

	jsonResponse, err = client.do()
	client.Close()
	return
}

// Close closes the database connection
func (c *Client) Close() {
	if c != nil {
		c = nil
	}
}

func (c *Client) newRequest() error {
	//compose endpoint
	endpoint := fmt.Sprintf("%s%s", BASEENDPOINT, c.EndpointURL)
	//add parameters query
	endpoint, err := addQueryParams(endpoint, c.UrlQuery)
	if err != nil {
		return err
	}

	u, err := url.Parse(endpoint)
	if err != nil {
		return err
	}

	//get a new *http.Request base on endpoint
	c.req, err = http.NewRequest("GET", u.String(), nil)
	if err != nil {
		return err
	}

	return nil
}

func (c *Client) do() (jsonResponse interface{}, err error) {
	var resp *http.Response
	// Set default headers
	c.req.Header.Set("Accept", "application/json")

	//execute request
	httpClient := &http.Client{
		Timeout: time.Second * 30,
	}
	resp, err = httpClient.Do(c.req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//check response from error
	err = checkResponse(resp)
	if err != nil {
		return nil, err
	}

	json.NewDecoder(resp.Body).Decode(&jsonResponse)

	return
}

func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("%v %v: %d %v",
		r.Response.Request.Method, r.Response.Request.URL, r.Response.StatusCode, r.Message)
}

//Add URL query parameters
func addQueryParams(s string, opt interface{}) (string, error) {
	v := reflect.ValueOf(opt)

	if v.Kind() == reflect.Ptr && v.IsNil() {
		return s, nil
	}

	origURL, err := url.Parse(s)
	if err != nil {
		return s, err
	}
	origValues := origURL.Query()

	newValues, err := query.Values(opt)
	if err != nil {
		return s, err
	}

	for k, v := range newValues {
		origValues[k] = v
	}

	origURL.RawQuery = origValues.Encode()
	return origURL.String(), nil
}

// CheckResponse checks the API response for errors, and returns them if present. A response is considered an
// error if it has a status code outside the 200 range. API error responses are expected to have either no response
// body, or a JSON response body that maps to ErrorResponse. Any other response body will be silently ignored.
func checkResponse(r *http.Response) error {
	if c := r.StatusCode; c >= 200 && c <= 299 {
		return nil
	}
	errorResponse := &ErrorResponse{Response: r}
	data, err := ioutil.ReadAll(r.Body)
	if err == nil && len(data) > 0 {
		err := json.Unmarshal(data, errorResponse)
		if err != nil {
			errorResponse.Message = string(data)
		}
	}

	return errorResponse
}
