package api

import (
	"encoding/json"
	"fmt"
	"errors"
)

type Rover struct {
	ID          int      `json:"id"`
	Name        string   `json:"name"`
	LandingDate string   `json:"landing_date"`
	LaunchDate  string   `json:"launch_date"`
	Status      string   `json:"status"`
	MaxSol      int      `json:"max_sol"`
	MaxDate     string   `json:"max_date"`
	TotalPhotos int      `json:"total_photos"`
	Cameras     []Camera `json:"cameras"`
}

//GetRovers get all rovers
//example enpoint https://api.nasa.gov/mars-photos/api/v1/rovers?api_key=aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe
func (c ClientNasa) GetRovers() (*[]Rover, error) {
	//url paramaters
	var options = struct {
		ApiKey string `url:"api_key"`
	}{c.ApiKey}

	//execute request
	resp, err := c.Get("rovers", options)
	if err != nil {
		return nil, err
	}
	//create anonymous struct to store rovers
	rovers := struct {
		Rovers []Rover
	}{}
	respB, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respB, &rovers)
	if err != nil {
		return nil, err
	}
	return &rovers.Rovers, nil
}

//GetPhotosFromRoverCamera - get rover photos from specific camera
//example enpoint https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&camera=NAVCAM&api_key=aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe
func (c ClientNasa) GetPhotosFromRoverCamera(roverName, cameraName string, page int) (*[]Photo, error) {
	if roverName == "" {
		return nil, errors.New("rover name is required")
	}
	if cameraName == "" {
		return nil, errors.New("camera name is required")
	}
	endpointForRover := fmt.Sprintf("rovers/%s/photos", roverName)
	//url paramaters
	var options = struct {
		ApiKey string `url:"api_key"`
		Camera string `url:"camera"`
		Sol    int    `url:"sol"`
		Page   int    `url:"page"`
	}{c.ApiKey, cameraName, 1000, page}

	//execute request
	resp, err := c.Get(endpointForRover, options)
	if err != nil {
		return nil, err
	}
	//create anonymous struct to store photos
	photos := struct {
		Photos []Photo
	}{}
	respB, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respB, &photos)
	if err != nil {
		return nil, err
	}
	return &photos.Photos, nil
}

//GetPhotosFromRoverCamera - get rover photos from specific camera
//example enpoint https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&camera=NAVCAM&api_key=aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe
func (c ClientNasa) GetPhotosFromRoverAllCameras(roverName string, page int) (*[]Photo, error) {
	if roverName == "" {
		return nil, errors.New("rover name is required")
	}

	endpointForRover := fmt.Sprintf("rovers/%s/photos", roverName)
	//url paramaters
	var options = struct {
		ApiKey string `url:"api_key"`
		Sol    int    `url:"sol"`
		Page   int    `url:"page"`
	}{c.ApiKey, 1000, page}

	//execute request
	resp, err := c.Get(endpointForRover, options)
	if err != nil {
		return nil, err
	}
	//create anonymous struct to store photos
	photos := struct {
		Photos []Photo
	}{}
	respB, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respB, &photos)
	if err != nil {
		return nil, err
	}
	return &photos.Photos, nil
}

//GetPhotosFromRoverOnDate - get rover photos from specific date
//example enpoint  https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?earth_date=2015-6-3&api_key=DEMO_KEY
func (c ClientNasa) GetPhotosFromRoverOnDate(roverName, date string, page int) (*[]Photo, error) {
	if roverName == "" {
		return nil, errors.New("rover name is required")
	}
	if date == "" {
		return nil, errors.New("date of photos is required")
	}

	endpointForRover := fmt.Sprintf("rovers/%s/photos", roverName)
	//url paramaters
	var options = struct {
		ApiKey    string `url:"api_key"`
		EarthDate string `url:"earth_date"`
		Page      int    `url:"page"`
	}{c.ApiKey, date, page}

	//execute request
	resp, err := c.Get(endpointForRover, options)
	if err != nil {
		return nil, err
	}
	//create anonymous struct to store photos
	photos := struct {
		Photos []Photo
	}{}
	respB, err := json.Marshal(resp)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(respB, &photos)
	if err != nil {
		return nil, err
	}
	return &photos.Photos, nil
}
