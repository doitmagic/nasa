package api

import (
	"errors"
)

const BASEENDPOINT = "https://api.nasa.gov/mars-photos/api/v1/"

// ClientNasa manages communication with Nasa Open API's.
type ClientNasa struct {
	ApiKey string
	Rover
}

func NewNASAClient(apiKey string) (*ClientNasa, error) {
	if apiKey == "" {
		return nil, errors.New("apiKey is required to create to use nasa API")
	}
	return &ClientNasa{
		ApiKey: apiKey,
	}, nil
}
