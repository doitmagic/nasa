package api_test

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"bitbucket.org/doitmagic/nasa/pkg/api"
	"testing"
)

var _ = Describe("Api", func() {
	Context("Test API", func() {
		var nasaClient *api.ClientNasa

		It("Sould return nasaClient", func() {
			var err error
			nasaClient, err = api.NewNASAClient("aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe")
			Expect(err).NotTo(HaveOccurred())
			Expect(nasaClient.ApiKey).To(Equal("aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe"))
		})
		It("Sould return 3 rovers", func() {
			rovers, err := nasaClient.GetRovers()
			Expect(err).NotTo(HaveOccurred())
			Expect(len(*rovers)).To(Equal(3))
		})
		It("Sould return photos from curiosity rover, camera NAVCAM", func() {
			photos, err := nasaClient.GetPhotosFromRoverCamera("curiosity", "NAVCAM", 1)
			Expect(err).NotTo(HaveOccurred())
			Expect(len(*photos)).NotTo(Equal(0))
			Expect((*photos)[0].ID).To(Equal(103383))
		})
		It("Sould return photos from curiosity rover, camera MAST, page 2", func() {
			photos, err := nasaClient.GetPhotosFromRoverCamera("curiosity", "MAST", 2)
			Expect(err).NotTo(HaveOccurred())
			Expect(len(*photos)).NotTo(Equal(0))
			Expect((*photos)[0].ID).To(Equal(424930))
		})
		It("Sould return photos from data 2015-6-3", func() {
			photos, err := nasaClient.GetPhotosFromRoverOnDate("curiosity", "2015-6-3", 1)
			Expect(err).NotTo(HaveOccurred())
			Expect(len(*photos)).To(Equal(4))
		})
		It("Sould return photos from Opportunity rover from data 2015-6-3", func() {
			photos, err := nasaClient.GetPhotosFromRoverOnDate("opportunity", "2015-6-3", 1)
			Expect(err).NotTo(HaveOccurred())
			Expect(len(*photos)).To(Equal(25))
		})
	})
})

func benchmarkPhotos(roverName string, b *testing.B) {
	nasaClient, _ := api.NewNASAClient("aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe")
	for n := 0; n < b.N; n++ {
		nasaClient.GetPhotosFromRoverCamera(roverName, "NAVCAM", 1)
	}
}

func benchmarkPhotosFromDate(roverName, date string, b *testing.B) {
	nasaClient, _ := api.NewNASAClient("aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe")
	for n := 0; n < b.N; n++ {
		nasaClient.GetPhotosFromRoverOnDate(roverName, date, 1)
	}
}

func BenchmarkPhotosCuriosity(b *testing.B)   { benchmarkPhotos("curiosity", b) }
func BenchmarkPhotosOpportunity(b *testing.B) { benchmarkPhotos("opportunity", b) }
func BenchmarkPhotosForDate(b *testing.B)     { benchmarkPhotosFromDate("opportunity", "2015-6-3", b) }
