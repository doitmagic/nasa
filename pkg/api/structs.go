package api

type PhotoFull struct {
	ID        int    `json:"id"`
	Sol       int    `json:"sol"`
	Camera    Camera `json:"camera"`
	ImgSrc    string `json:"img_src"`
	EarthDate string `json:"earth_date"`
	Rover     Rover  `json:"rover"`
}

type Photo struct {
	ID        int    `json:"id"`
	ImgSrc    string `json:"img_src"`
	EarthDate string `json:"earth_date"`
}

type Camera struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	RoverID  int    `json:"rover_id"`
	FullName string `json:"full_name"`
}

type Cameras []struct {
	Name     string `json:"name"`
	FullName string `json:"full_name"`
}
