package server

import (
	"html/template"
	"io"
	"github.com/labstack/echo"
	"path"
	"runtime"
	"github.com/labstack/echo/middleware"
	. "github.com/logrusorgru/aurora"
	"fmt"
	"os"
	"time"
	"context"
)

// TemplateRenderer is a custom html/template renderer for Echo framework
type TemplateRenderer struct {
	templates *template.Template
}

// Render renders a template document
func (t *TemplateRenderer) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	// Add global methods if data is a map
	if viewContext, isMap := data.(map[string]interface{}); isMap {
		viewContext["reverse"] = c.Echo().Reverse
	}
	return t.templates.ExecuteTemplate(w, name, data)
}

func Start() {
	e := echo.New()
	e.Logger.SetLevel(10)
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "" + Sprintf("%s", Green("time")) + ":${time_rfc3339}, " + Sprintf("%s", Green("uri")) + Sprintf("%s", Brown(":${uri},")) + Sprintf("%s", Green("latency")) + Sprintf("%s", Blue(":${latency_human},")) + Sprintf("%s", Green("remote_ip")) + ":${remote_ip}, " + Sprintf("%s", Green("bytes_out")) + ":${bytes_out}," + Sprintf("%s", Cyan("method")) + ":${method}:${status}\n",
	}))
	renderer := &TemplateRenderer{
		templates: template.Must(template.ParseGlob(base() + "/templates/*.html")),
	}
	e.Renderer = renderer
	e.Use(middleware.Static(base() + "/assets"))
	e.Static("/assets", base()+"/assets")
	Load(e)

	go func() {
		fmt.Println("Start server: :8895")
		err := e.Start(":8895")
		if err != nil {
			e.Logger.Fatal(err)
		}
	}()

	quit := make(chan os.Signal)

	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}

}

func base() string {
	_, filename, _, _ := runtime.Caller(1)
	filepath := path.Join(path.Dir(filename), "./")
	return filepath
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	// For info on each, see: https://golang.org/pkg/runtime/#MemStats
	fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	fmt.Printf("\tNumGC = %v", m.NumGC)
	fmt.Printf("\tGroutines = %v\n", runtime.NumGoroutine())
}

func bToMb(b uint64) uint64 {
	return b / 1024 / 1024
}
