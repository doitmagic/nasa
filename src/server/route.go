package server

import (
	"github.com/labstack/echo"
	"net/http"
	"bitbucket.org/doitmagic/nasa/pkg/api"
	"log"
	"strconv"

	"bitbucket.org/doitmagic/nasa/src/server/appcontext"
)

func Load(e *echo.Echo) {

	appcontextInitial := appcontext.GetInstance()
	nasaFirstClient, err := api.NewNASAClient("aaBlgQwnyQBC5G3bW553dMoZ96U4hYR7ZIhkfmPe")
	if err != nil {
		log.Fatal(err)
	}
	rovers, err := nasaFirstClient.GetRovers()
	appcontextInitial.SetRovers(rovers)
	appcontextInitial.SetNasaClient(nasaFirstClient)

	e.GET("/", func(c echo.Context) error {
		nasaClient := appcontextInitial.GetNasaClient()
		photos, err := nasaClient.GetPhotosFromRoverCamera("curiosity", "NAVCAM", 1)
		if err != nil {
			log.Fatal(err)
		}

		return c.Render(http.StatusOK, "home.html", map[string]interface{}{
			"rovers":       appcontextInitial.GetRovers(),
			"photos":       *photos,
			"rover":        "curiosity",
			"cam":          "NAVCAM",
			"page":         c.Param("page"),
			"previouspage": 1,
			"nextpage":     2,
		})
	}).Name = "home"

	e.GET("/rover/:rover/:camera/:page", func(c echo.Context) error {
		page, err := strconv.Atoi(c.Param("page"))
		nasaClient := appcontextInitial.GetNasaClient()
		photos, err := nasaClient.GetPhotosFromRoverCamera(c.Param("rover"), c.Param("camera"), page)
		if err != nil {
			log.Fatal(err)
		}
		PrintMemUsage()
		return c.Render(http.StatusOK, "home.html", map[string]interface{}{
			"rovers":       appcontextInitial.GetRovers(),
			"photos":       *photos,
			"rover":        c.Param("rover"),
			"cam":          c.Param("camera"),
			"page":         c.Param("page"),
			"previouspage": strconv.Itoa(page - 1),
			"nextpage":     strconv.Itoa(page + 1),
		})
	}).Name = "home"

	e.GET("/rover/:rover/cameras/:page", func(c echo.Context) error {
		page, err := strconv.Atoi(c.Param("page"))
		nasaClient := appcontextInitial.GetNasaClient()
		photos, err := nasaClient.GetPhotosFromRoverAllCameras(c.Param("rover"), page)
		if err != nil {
			log.Fatal(err)
		}
		PrintMemUsage()
		return c.Render(http.StatusOK, "home.html", map[string]interface{}{
			"rovers":       appcontextInitial.GetRovers(),
			"photos":       *photos,
			"rover":        c.Param("rover"),
			"cam":          "all",
			"page":         c.Param("page"),
			"previouspage": strconv.Itoa(page - 1),
			"nextpage":     strconv.Itoa(page + 1),
		})
	}).Name = "home"

}
