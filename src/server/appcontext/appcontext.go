package appcontext //singleton-pattern-in-go

import (
	"sync"
	"bitbucket.org/doitmagic/nasa/pkg/api"
)

// Context is the app context
type Context struct {
	rovers     *[]api.Rover
	nasaClient *api.ClientNasa
}

//SetRovers can only set the Rovers one time for all web app
func (c *Context) SetRovers(rovers *[]api.Rover) {
	if c.rovers == nil {
		c.rovers = rovers
	}
}

//SetNasaClient can only set the Nasa client one time for all web app
func (c *Context) SetNasaClient(nasaClient *api.ClientNasa) {
	if c.nasaClient == nil {
		c.nasaClient = nasaClient
	}
}

//GetRovers returns the Rovers
func (c *Context) GetRovers() *[]api.Rover {
	return c.rovers
}

//GetNasaClient returns the NasaClient
func (c *Context) GetNasaClient() *api.ClientNasa {
	return c.nasaClient
}

var instance *Context
var once sync.Once

//GetInstance returns the same context every time
func GetInstance() *Context {
	once.Do(func() {
		instance = &Context{}
	})
	return instance
}
